#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This scripts broadcasts the private IP of the client to the server.
import time, requests, netifaces
from vars_global import BaseURL, StationID, interface, IP_BROADCAST_PERIOD

def getLocalIP():
	if interface in netifaces.interfaces():
		if len(netifaces.ifaddresses(interface)) > 2:
			ip = netifaces.ifaddresses(interface)[2][0]['addr']
			response = ip
		else:
			response = None
	else:
		response = None

	return response

def broadcastIP(stationID, ip):
	data = {'stationID':stationID,'ip':ip}	
	url = 'http://' + BaseURL + '/stations'	
	try:
		r = requests.post(url, json=data)
	except requests.exceptions.RequestException as e:
		print e

while True:
	broadcastIP(StationID,getLocalIP())
	time.sleep(IP_BROADCAST_PERIOD)