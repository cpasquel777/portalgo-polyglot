#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Vera RPIClient v0.1
#This scripts runs in the RPi. Detects beacons and notify the server
import time, requests
from beacontools import BeaconScanner, IBeaconFilter
from vars_global import BaseURL, StationID, BeaconsList

def updateServer(beaconID, stationID, rssi):
	data = {'beaconID':beaconID,'stationID':stationID,'rssi':rssi}	
	url = 'http://' + BaseURL + '/beacons'	
	try:
		r = requests.post(url, json=data)
	except requests.exceptions.RequestException as e:
		print e

def callback(bt_addr, rssi, packet, additional_info):
	beacons = BeaconsList
	minor = str(additional_info["minor"])
	if minor in beacons.keys():
		print(beacons[minor])
		updateServer(beacons[minor], StationID, rssi)
	else:
		print("unknown minor")
	

# scan for all iBeacon advertisements from beacons with the specified uuid 
scanner = BeaconScanner(callback,     
    device_filter=IBeaconFilter(uuid="b9407f30-f5f8-466e-aff9-25556b57fe6d")
)

scanner.start()
