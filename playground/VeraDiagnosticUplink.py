#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Vera 0.1 stations diagnostic script

import mysql.connector, json, os, requests, time
from mysql.connector import errorcode
from datetime import datetime

def get_station_info(conn, cursor, stationID):

	station = str(stationID)
	prequery = "SELECT TIMESTAMP FROM log WHERE STATION_ID = " + station + " ORDER BY TIMESTAMP DESC LIMIT 1;"	
	#prequery= "SELECT BEACON_ID FROM log WHERE TIMESTAMP = %s AND BEACON_ID = %s"
	prevals = (station)
	cursor.execute(prequery)
	res= cursor.fetchall()	
	return res[0][0]


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

fo=open(os.path.join(__location__, 'config.json'))
config = json.load(fo)
fo.close()

DB_NAME = config['Database']
DB_USER = config['DBUser']
DB_PWD = config['DBPassword']

conn = mysql.connector.connect(user=DB_USER, passwd=DB_PWD)
cursor = conn.cursor()
cursor.execute("USE {}".format(DB_NAME))

url = 'https://www.wolframcloud.com/obj/christianp/Vera/API/StationStatus'

for i in range(1, 7):
	timestamp = get_station_info(conn, cursor, i)
	requestData = {'timestamp':timestamp,'station':i, 'pwd': 'veraveravera'}
	r = requests.get(url, params=requestData)
#	print i
	time.sleep(2)

